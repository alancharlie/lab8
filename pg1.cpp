#include <iostream>
#include <string>
using namespace std;

class CustomerCounter{
    private:
    int maximum_customers;
    int customer_counter;
    public:
    CustomerCounter(int);
    void add(int);
    void subtract(int);
    void print();
};

 CustomerCounter::CustomerCounter(int max){
     this->customer_counter=0;
     this->maximum_customers=max;
 }
 
 void CustomerCounter::add(int n){
     if(this->customer_counter+n > this->maximum_customers)
         this->customer_counter = this->maximum_customers;
     else 
         this->customer_counter+=n;
 }
 
 void CustomerCounter::subtract(int n){
     if(this->customer_counter-n < 0)
         this->customer_counter = 0;
     else 
         this->customer_counter-=n;
 }
 
 void CustomerCounter::print(){
     cout<<"customer count: "<<this->customer_counter<<"\nMaximum customers: "<<this->maximum_customers<<endl;
 }

int main() {
   CustomerCounter cc(30);
   cout<<"Adding 15: \n";
   cc.add(15);
   cc.print();
   cout<<"Adding 17 more: \n";
   cc.add(13);
   cc.print();
   cout<<"Adding 5 more: \n";
   cc.add(5);
   cc.print();
   cout<<"Subtracting 5 : \n";
   cc.subtract(5);
   cc.print();
   
}