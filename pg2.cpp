#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
class Person{
    string name;
    vector<Person*>friends;
    public:
    Person(string);
    void befriend(Person*);
    void unfriend(Person*);

    void print();
};

Person::Person(string name) : name(name), friends(vector<Person*>()){};

void Person::befriend(Person* person){
  this->friends.push_back(person);
}

void Person::unfriend(Person* person){
  auto e = find(this->friends.begin(), this->friends.end(), person);
    if (e != this->friends.end()) {
        this->friends.erase(e);
    }
}

void Person::print() {
    cout << "\nName: " << this->name << "\nFriends: ";

    for (auto a : this->friends) {
        cout << a->name<<" ";
    }

    cout<<"\n"<<endl;
}

int main(){
    Person alan = Person("alan");
    Person vishnu = Person("vishnu");
    Person vismaya = Person("vismaya");

    cout << "[People] \n";
    alan.print();
    vishnu.print();
    vismaya.print();

    alan.befriend(&vishnu);
    alan.befriend(&vismaya);
    vishnu.befriend(&vismaya);
    vismaya.befriend(&vishnu);

    cout << "[befriended] \n";
    alan.print();
    vishnu.print();
    vismaya.print();

    cout << "[unfriending vishnu from alan] \n";
    alan.unfriend(&vishnu);
    alan.print();
}