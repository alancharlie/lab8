
#include <stdio.h>
#include <iostream>
#include <vector>
#define PI 3.14
using namespace std;

 class Shape{
    public:
    virtual double area()=0;
    
};

class Square : public Shape {
    
    double side;
    public:
    Square(double side) {
        this->side = side;
        area();
    }
    
     double area () override{
        return side*side;
    }
};

class Rectangle : public Shape {
    
    double length;
    double breadth;
    public:
    Rectangle(double length, double breadth){
        this->length=length;
        this->breadth=breadth;
        area();
    }
     double area() override{
         return length*breadth;
    }
};

 class Circle : public Shape {
    private:
    double radius;
    public:
    Circle(){}
    Circle(double radius){
        this->radius=radius;
        area();
    }
     double area() override {
        return PI*radius*radius;
    }
};

int main()
{
    vector <Shape*> shapes;
    Shape *c= new Circle(20);
    Shape *s= new Square(5);
    Shape *r= new Rectangle(8,3);
    shapes.push_back (c);
    shapes.push_back (s);
    shapes.push_back (r);
    
    for (int i = 0; i < shapes.size(); i++)
         cout << "area of " << i<< " is " << shapes[i]->area()<<"\n";
   
    return 0;
 }

